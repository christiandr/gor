<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     //
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    public function upload(Request $request, Product $product)
    {
        if($request->sync_type == 1 && $request->date_uploaded == null)
        {
            $newProduct = new Product;

            $newProduct->store_id               = $request->store_id;
            $newProduct->sku_id                 = $request->sku_id;
            $newProduct->sync_type              = 0;
            $newProduct->date_uploaded          = $request->date_uploaded;
            $newProduct->sync_publish_unpublish = $request->sync_publish_unpublish;
            $newProduct->for_delete             = $request->for_delete;
            $newProduct->sync_for_image         = $request->sync_for_image;
            $newProduct->publish_status         = $request->publish_status;

            $newProduct->save();
        } else {
            $updateProduct = Product::find();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function show(Product $product)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function edit(Product $product)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Product $product)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Product $product)
    // {
    //     //
    // }
    public function delete(Request $request, Product $product)
    {
        if($request->for_delete == 1){
            
            $deleteProduct->delete();
        }
    }
}
